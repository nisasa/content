---
title: "Johdanto, teknologiat ja versiohallinta"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---


```{r setup, include=FALSE}
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))
```

# Johdanto

Kurssilla käydään tiiviisti läpi R-kielen kenties suosituin data-analyysin prosessi josta käytetään nimeä *tidyverse*. Nimi viittaa [Hadley Wickhamin](http://hadley.nz/)  kirjoittamien pakettien ja [tidy data](https://cran.r-project.org/web/packages/tidyr/vignettes/tidy-data.html) -lähestymistavan muodostamaa kokonaisuutta (ks. alla oleva kuva).

![](http://courses.markuskainu.fi/utur2016/kuviot/wickham_cycle.png)

<blockquote>
  <p>The majority of the packages that you will learn in this book are part of the so-called tidyverse. All packages in the tidyverse share a common philosophy of data and R programming, which makes them fit together naturally. Because they are designed with a unifying vision, you should experience fewer problems when you combine multiple packages to solve real problems. The packages in the tidyverse are not perfect, but they fit together well, and over time that fit will continue to improve.</p>
  <p>There are many other excellent packages that are not part of the tidyverse, because they are designed with a different set of underlying principles. This doesn’t make them better or worse, just different. In other words, the complement to the tidyverse is not the messyverse, but many other universes of interrelated packages. As you tackle more data science projects with R, you’ll learn new packages and new ways of thinking about data. But we hope that the tidyverse will continue to provide a solid foundation no matter how far you go in R.</p>
  <small><a href="http://r4ds.had.co.nz/">Garrett Grolemund & Hadley Wickham (2016)</a> <cite title="Source Title">R for Data Science</cite></small>
</blockquote>

Kurssilla opitaan *tidy data* -lähestymistavan ohella myös ohjelmistokehityksen perustyökalujen käyttöä kuten versiohallintaa. Kaikki uudet työkalut otetaan käyttöön heti kurssin alussa ja ne opetellaan käytännön kautta.

## Miksi R:ää?

Kolme syytä:

1. lisensointi
2. lisensointi
3. lisensointi

```{r}
licence()
# This software is distributed under the terms of the GNU General
# Public License, either Version 2, June 1991 or Version 3, June 2007.
# The terms of version 2 of the license are in a file called COPYING
# which you should have received with
# this software and which can be displayed by RShowDoc("COPYING").
# Version 3 of the license can be displayed by RShowDoc("GPL-3").
#
# Copies of both versions 2 and 3 of the license can be found
# at https://www.R-project.org/Licenses/.
#
# A small number of files (the API header files listed in
# R_DOC_DIR/COPYRIGHTS) are distributed under the
# LESSER GNU GENERAL PUBLIC LICENSE, version 2.1 or later.
# This can be displayed by RShowDoc("LGPL-2.1"),
# or obtained at the URI given.
# Version 3 of the license can be displayed by RShowDoc("LGPL-3").
#
# 'Share and Enjoy.'
```

*Erityisesti Linux kernelistä tunnettu GNU General Public License (GPL) lienee ehkä kuuluisin vapaan lähdekoodin lisenssi. GPL on esimerkki copyleft (käyttäjänoikeus) lisenssistä. Copyleft-lisenssit ovat vapaan lähdekoodin lisenssejä jotka vaativat, että johdannaisteokset ovat myös copyleft-lisenssin alaisia. Copyleft-lisenssejä sanotaan tämän vuoksi myös tarttuviksi lisensseiksi. Kuten suomenkielinen nimi antaa ymmärtää, copyleftin tarkoitus on suojella loppukäyttäjää. Se tekee tämän kieltämällä uusien rajoitteiden lisäämisen lisenssiin ja näin kannustamalla kehittäjiä suosimaan vapaata lähdekoodia.*

*GPL sallii teoksen levityksen ja muokkauksen, kunhan seuraavia ehtoja noudatetaan:*

- *Lisenssiä ei saa muuttaa tai poistaa*
- *Muokkauksista pitää lisätä huomio*
- *Ohjelman tulee sisältää tieto lisenssistä*
- *Lähdekoodi pitää tehdä saataville kaikille, joille ohjelma on levitetty*
- *Johdannaisteokset tulee lisensoida samalla lisenssillä*

*GPL ei ole kuitenkaan EULA, eli se ei koske ohjelmiston käyttäjää. Ehdot koskevat ainoastaan levittäjiä. Omia muutoksia ei myöskään tarvitse julkaista kenelle tahansa, vaan ainoastaan niille kenelle muokatusta ohjelmasta on annettu kopio. Eli sisäiseen käyttöön tehtyjä muutoksia ei ole pakko julkaista.*

**Lähde: Sofokus: [Avoin lähdekoodi – Tiedätkö mitä rajoituksia se asettaa ohjelmistosi käytölle?](https://www.sofokus.com/blogi/avoin-lahdekoodi/)**

## Avoimen lisensoinnin seuraukset

- Contributed packages <https://cran.r-project.org/web/packages/>
- On the growth of CRAN packages <http://blog.revolutionanalytics.com/2016/04/cran-package-growth.html>
- The Popularity of Data Analysis Software <http://r4stats.com/articles/popularity/>
- Annual Dice Tech Salary Survey:
    - 2014 <http://marketing.dice.com/pdf/Dice_TechSalarySurvey_2014.pdf>
    - 2015 <http://marketing.dice.com/pdf/Dice_TechSalarySurvey_2015.pdf>
    - 2016 <http://marketing.dice.com/pdf/Dice_TechSalarySurvey_2016.pdf>

- [Elokuun 29., 2016: Video series: Introduction to Microsoft R Server](http://blog.revolutionanalytics.com/2016/08/introduction-to-microsoft-r-server.html)
- [Elokuun 26., 2016: Microsoft R Open 3.3.1 now available for Windows, Mac and Linux](http://blog.revolutionanalytics.com/2016/08/microsoft-r-open-331-now-available-for-windows-mac-and-linux.html)
- [Kesäkuun 6., 2016: IBM Invests in R Programming Language for Data Science; Joins R Consortium](https://www.r-consortium.org/news/announcement/2016/06/ibm-invests-r-programming-language-data-science-joins-r-consortium)
- [Joulukuun 25, 2015:  Oracle R Enterprise 1.5 Released](https://blogs.oracle.com/R/entry/oracle_r_enterprise_1_5)






# Teknologiat ja ohjelmistoympäristö


Kurssilla hyödynnetään [Tieteen tietotekniikan keskuksen (CSC)](http://csc,fi) *Pouta Blueprints*  laskentaympäristö, joita opiskelijat voivat käyttää haka-verkon kautta pelkkää selainta käyttäen joko mikroluokkien koneilla tai omalla koneella. Kurssin edetessä halukkaat voivat rakentaa myös vastaavan ympäristön omalle koneelleen omatoimista käyttöä varten.


Kurssilla käytettävät ohjelmistot ovat poikkeuksetta [vapaita](https://fi.wikipedia.org/wiki/Vapaa_ohjelmisto) ja [avoimen lähdekoodin ohjelmistoja](https://fi.wikipedia.org/wiki/Avoin_l%C3%A4hdekoodi), joita voi käyttää sekä Windowsissa, OSX:ssä että eri linux-jakeluissa.

Ohjelmistoympäristö on perusteltu ja käyty läpi hyvin täällä: [Jenny Bryan and the STAT 545 TAs: Happy Git and GitHub for the useR](http://happygitwithr.com/)


## R

<a href="http://r-project.org/">
<img border="0" alt="W3Schools" style="float:right;width:70px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/R_logo.svg/310px-R_logo.svg.png" >
</a>

- tieteellisen laskennan ja tilastollisen analyysin ohjelmointikieli
- Opettele R:ää <https://www.rstudio.com/online-learning/>

## Rstudio

<a href="https://rstudio.com/">
<img border="0" alt="W3Schools" style="float:right;width:150px;" src="https://upload.wikimedia.org/wikipedia/en/thumb/f/f0/RStudio_logo.png/320px-RStudio_logo.png">
</a>

- IDE eli käyttöliittymä R:lle
- RStudio Full Tour <https://vimeo.com/97378167>

### Rmarkdown

- literate programming -merkintäkieli
- What is R Markdown? <https://vimeo.com/177254549>
- Communicate-better-with-R-Markdown  <https://vimeo.com/176881928>
- Getting started with R Markdown  <https://vimeo.com/142172484>

## git

<a href="https://git-scm.com/">
<img border="0" alt="W3Schools" style="float:right;width:150px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/320px-Git-logo.svg.png">
</a>

- versiohallinnan teknologia
- git <https://git-scm.com/>
- The Basics of Git and GitHub <https://www.youtube.com/watch?v=U8GBXvdmHT4>
- Lear Git in 20 minutes <https://www.youtube.com/watch?v=Y9XZQO1n_7c>
- RStudio & git/github Demonstration <https://vimeo.com/119403806>

## Gitlab

<a href="https://gitlab.com/">
<img border="0" alt="W3Schools" style="float:right;width:150px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/GitLab_logo.png/320px-GitLab_logo.png">
</a>

- versiohallintaa (git) tukeva koodin jakamisen verkkopalvelu
- [Gitlab](https://gitlab.com) ei ole ohjelmisto, vaan kenties suosituin lähdekoodin jakamisen ja yhdessä ohjelmoimisen (social coding) mahdollistava verkkopalvelu.
- Kurssille osallistuminen edellyttää Gitlab:iin rekisteröitymistä.
- Syy miksi emme käytä [Github](http://github.com/) on se, että Gitlab:ssa on oletuksena tarjolla rajaton määrä privaatteja projekteja. Se voi olla monissa tutkimusprojekteissa tarpeen.

## Slack

<a href="https://slack.com/">
<img border="0" alt="W3Schools" style="float:right;width:150px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Slack_CMYK.svg/320px-Slack_CMYK.svg.png">
</a>

- Kommunikaatiokanava tiimeille
- Miksi: koska pop ja hyvä gitlab-integraatio
- [What Slack is doing to our offices—and our minds](http://arstechnica.com/information-technology/2016/03/what-slack-is-doing-to-our-offices-and-our-minds/)


## shell ie. komentorivi

- tekstipohjainen käyttöliittymä käyttöjärjestelmän eri palveluihin/ohjelmiin
- <https://en.wikipedia.org/wiki/Shell_(computing)>
- <https://en.wikipedia.org/wiki/Shell_script>


# Johdanto versiohallintaan

- [gitbook: Alkusanat - Versionhallinnasta](https://git-scm.com/book/fi/v1/Alkusanat-Versionhallinnasta)

## git

Luettavaa!

- [gitbook - Alkusanat - Gitin lyhyt historia](https://git-scm.com/book/fi/v1/Alkusanat-Gitin-lyhyt-historia)
- [gitbook - Gitin perusteet](https://git-scm.com/book/fi/v1/Gitin-perusteet)
- [TIE-02200 Ohjelmoinnin peruskurssi - Git ja versionhallinta](http://www.cs.tut.fi/~opersk/S2015/@wrapper.shtml?materiaalit/git)


